<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'bank_api' => [
        'seven_bank' => [
            'headers' => [
                'Ocp-Apim-Subscription-key' => env('OCP_APIM_SUBSCRIPTION_KEY'),
                'X-Partner-Code' => env('X_PARTNER_CODE'),
                'Content-Type' => 'application/json; charset=utf-8',
                'User-Agent' => env('USER_AGENT'),
            ],
            'hash_key' => env('HASH_KEY'),
            'url' => env('BANK_URL'),
        ],
        'japan_net_bank' => [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded; charset=utf-8',
                'content-length' => env('JNB_CONTENT_LENGTH'),
            ],
            'url' => env('JNB_URL'),
            'HD_VL' => env('JNB_HD_VL'),
            'pattern-code' => env('JNB_PATTERN_CODE'),
            'wait_time_retry' => env('JNB_WAIT_TIME_RETRY'),
            'hd_tran_id' => env('JNB_HD_TRANID'),
            'trans_id' => env('JNB_TRANSID'),
        ],
        'retry_number' => env('BANK_API_RETRY_NUMBER', 3),
    ],
];
