<?php

namespace App\Providers;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Response as ResponseStatus;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Support\ServiceProvider;
use Log;

class ResponseServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerSuccessResponse();
        $this->registerErrorResponse();
    }

    /**
     * Register success response
     *
     * @return void
     */
    private function registerSuccessResponse()
    {
        Response::macro('success', function (
            $message,
            $data = [],
            $statusCode = ResponseStatus::HTTP_OK,
            $headers = []
        ) {
            return response()->json([
                'status_code' => $statusCode,
                'message' => $message,
                'data' => $data,
            ], $statusCode, $headers);
        });
    }

    /**
     * Register error response
     *
     * @return void
     */
    private function registerErrorResponse()
    {
        Response::macro('error', function (
            $message,
            $exception,
            $statusCode = ResponseStatus::HTTP_INTERNAL_SERVER_ERROR,
            $headers = []
        ) {
            $errorCode = !empty($exception->getCode()) ? $exception->getCode() : ResponseStatus::HTTP_INTERNAL_SERVER_ERROR;

            switch (true) {
                case $exception instanceof AuthenticationException:
                    $message = trans('messages.status_code.401');
                    $statusCode = ResponseStatus::HTTP_UNAUTHORIZED;
                    $errorCode = ResponseStatus::HTTP_UNAUTHORIZED;
                    break;
                case $exception instanceof ModelNotFoundException:
                    $message = trans('messages.status_code.404');
                    $statusCode = ResponseStatus::HTTP_NOT_FOUND;
                    break;
                case $exception instanceof ValidatorException:
                    $message = trans('messages.status_code.422');
                    $statusCode = ResponseStatus::HTTP_UNPROCESSABLE_ENTITY;
                    break;
                default:
                    Log::error($exception);
            }

            return response()->json([
                'status_code' => $statusCode,
                'message' => $message,
                'error_code' => $errorCode,
            ], $statusCode, $headers);
        });
    }
}
