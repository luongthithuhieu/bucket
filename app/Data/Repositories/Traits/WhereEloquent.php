<?php

namespace App\Data\Repositories\Traits;

trait WhereEloquent
{

    /**
     * Where not null
     *
     * @param string $columns Columns.
     *
     * @return $this
     */
    public function whereNotNull(string $columns)
    {
        $this->model = $this->model->whereNotNull($columns);

        return $this;
    }

    /**
     * Where null
     *
     * @param string $columns Columns.
     *
     * @return $this
     */
    public function whereNull(string $columns)
    {
        $this->model = $this->model->whereNull($columns);

        return $this;
    }

    /**
     * Where data by field and value
     *
     * @param string $field    Field.
     * @param mixed  $value    Value.
     * @param string $operator Operator.
     *
     * @return $this
     */
    public function whereByField(string $field, $value = null, string $operator = '=')
    {
        $this->model = $this->model->where($field, $operator, $value);

        return $this;
    }
}
