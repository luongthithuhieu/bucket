<?php

if (!function_exists('currentUserLogin')) {

    /**
     * Get current user login
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    function currentUserLogin()
    {
        return \Illuminate\Support\Facades\Auth::user();
    }
}

